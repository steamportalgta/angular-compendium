import { Component } from '@angular/core';
import { Observable, of, map, mergeMap, filter } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  // Original Observable
  numbers$ : Observable<number> = of(1,2,3,4,5);

  // Values for help
  mapObs$: any;
  mergeMapObs$: any;
  filterObs$: any;

  onMapClick(): void{
    this.mapObs$ = this.numbers$.pipe(
      // changing the returned value into squared value
      map(number => {
        return number * number;
      })
    )
    this.mapObs$.subscribe(console.log);
  }

  onMergeMapClick(): void {
    this.mergeMapObs$ = this.numbers$.pipe(
      // returning new observable to subscribe instead original one
      mergeMap(number => {
        return of(number + 1)
      }
    ));

    this.mergeMapObs$.subscribe(console.log);
  }

  onFilterClick(): void {
    this.filterObs$ = this.numbers$.pipe(
      filter(number => number < 3)
    );

    this.filterObs$.subscribe(console.log)
  }
}


